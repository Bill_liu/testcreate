//
//  TCAppDelegate.h
//  testCreate
//
//  Created by liu min on 13-5-4.
//  Copyright (c) 2013年 sportsexp. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TCViewController;

@interface TCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TCViewController *viewController;

@end
