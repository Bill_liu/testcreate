//
//  main.m
//  testCreate
//
//  Created by liu min on 13-5-4.
//  Copyright (c) 2013年 sportsexp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TCAppDelegate class]));
    }
}
